<?php  //Template Name: Left Sidebar Page ?>
<?php get_template_part('pink','header')?>
<!-- Container for products -->
<div class="container">
		 <!-- Main --> 
		 <div class="_blank"></div>
			<div class="row-fluid" style="margin: 0px;">
					<div class="<?php if(!is_active_sidebar('sidebar-primary')){ echo 'col-md-12'; }else { echo 'col-md-8'; } ?>">
						<?php the_post();?>
							<h2 class="blog_detail_head"><?php the_title(); ?></h2>
							 <div class="media">
								<div class="media-body">
									 <div class="blog-detail-content"><p><?php the_content(); ?> </p></div>
								</div>
							</div>
					</div>
			</div>
</div><?php get_footer();?>
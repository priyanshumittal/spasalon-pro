<?php get_template_part('pink','header'); ?>
<?php $current_options=get_option('spa_theme_options'); ?>
<!-- Service --> 
<?php $term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') ); ?>
<div class="container"> 
     <div class="row">
	  <div class="span12 service-2c-xpt">
				   <h2 class="services-two-heading" ><?php echo $term->name;  ?></h2>
				   </div>
		<?php 
		$j=1; ?>
		<?php while(have_posts()): the_post();?>
		<?php if($current_options['tax_service_item']==1) { ?>
			<div class="span12 service-1c-xpt">
				<div class="media service-1c">
					<?php $defalt_arg =array('class' => "img-responsive" )?>
					<?php if(has_post_thumbnail()):?>
					   <a class="pull-left" href="<?php the_permalink(); ?>"title="<?php the_title(); ?>"><?php the_post_thumbnail('service-one-thumb', $defalt_arg); ?></a>
						<?php endif;?>
						<div class="media-body">
							<h4 class="service-media-heading"><a class="service-media-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<div class="services-content"><p><?php echo get_the_other_excerpt(); ?></p></div><br/>
								 <?php  $my_meta = get_post_meta($post->ID,'_my_meta',TRUE); ?>
						</div>
				</div>
				</div>
				<?php	} ?>
					
				<?php if($current_options['tax_service_item']==2) { ?>
			<div class="span6" style="margin-bottom:60px;" >
				<div class="media">
					<?php $defalt_arg =array('class' => "service_one_img" )?>
					<?php if(has_post_thumbnail()):?>
					   <a class="pull-left" href="<?php the_permalink(); ?>"title="<?php the_title(); ?>"><?php the_post_thumbnail('service-two-thumb', $defalt_arg); ?></a>
						<?php endif;?>
						<div class="media-body">
						<h4 id="service-media-heading">
						<a class="service-media-heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<div class="services-content"><p><?php echo get_service_two_excerpt(); ?></p></div>
						<?php  $my_meta = get_post_meta($post->ID,'_my_meta',TRUE); ?>
						</div>
				</div>
				</div>
				<?php	} ?>
					
				<?php  if($j%$current_options['tax_service_item']==0){ echo "<div class='clearfix'></div>"; } $j++; endwhile; ?>
	</div>
</div>
<?php  get_footer(); ?>
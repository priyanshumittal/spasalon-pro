<?php 
	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
	
	add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
	add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);
	function my_theme_wrapper_start() { 
		get_template_part('pink','header');?>
	<div class="container"><div class="_blank"></div><div class="row-fluid">
	<div class="<?php if(is_active_sidebar('woo-widget-area')) echo "span8"; else echo "span12";?>">
	<?php } 
	function my_theme_wrapper_end() {
	if( is_active_sidebar('woo-widget-area')){ echo "</div>"; get_sidebar('woo-widget-area'); echo "</div></div>"; }		
	echo '</div></div></div>';	} ?>
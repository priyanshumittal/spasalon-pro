<?php 
	define('WEBRITI_TEMPLATE_DIR',get_template_directory());
	define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/functions');
	
	
	//code for shoercode .....................
	define( 'WEBR_FRAMEWORK_DIR', get_template_directory_uri().'/functions' ); 

require_once('functions/webr_framework.php');
//wp title tag starts here
	function spa_head( $title, $sep )
	{	global $paged, $page;		
		if ( is_feed() )
			return $title;
		// Add the site name.
		$title .= get_bloginfo( 'name' );
		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( _e( 'Page', 'sis_spa' ), max( $paged, $page ) );
		return $title;
	}	
	add_filter( 'wp_title', 'spa_head', 10, 2 );
// code for default data 

if ( ! isset( $content_width ) ) $content_width = 900;

		require_once ( WEBRITI_THEME_FUNCTIONS_PATH . '/Menu_Walker/default_menu_walker.php' );//default menu 
		require_once ( WEBRITI_THEME_FUNCTIONS_PATH. '/Menu_Walker/spasalon_nav_walker.php' );//custom menu
		require_once ( WEBRITI_THEME_FUNCTIONS_PATH . '/Excerpt/excerpt_length.php' );// code for limit the length of excerpt
		require_once ( WEBRITI_THEME_FUNCTIONS_PATH. '/Pagination/pagination.php' );
		require_once ( WEBRITI_THEME_FUNCTIONS_PATH . '/Pagination/webriti_pagination.php' );
		require_once(WEBRITI_THEME_FUNCTIONS_PATH .'/scripts/scripts.php');
		require_once(get_template_directory() .'/default_data.php');
		require_once(WEBRITI_THEME_FUNCTIONS_PATH .'/cpt/cpt.php');
		require_once(WEBRITI_THEME_FUNCTIONS_PATH.'/meta-box/metabox.php');
		require_once(WEBRITI_THEME_FUNCTIONS_PATH.'/comments/commentbox.php');
		require_once(WEBRITI_THEME_FUNCTIONS_PATH.'/woo/woocommerce.php');
		 
			function spa_widgets_init() {
			/*sidebar*/
			register_sidebar( array(
					'name' => __( ' Sidebar', 'sis_spa' ),
					'id' => 'sidebar-primary',
					'description' => __( 'The primary widget area', 'sis_spa' ),
					'before_widget' => ' <div class="sidebar-widget">',
					'after_widget' => '</div>',
					'before_title' => ' <div class="sidebar-widget-title"><h4>',
					'after_title' => '</h4></div>',
				) );
			register_sidebar( array(
					'name' => __( 'Footer Widget Area', 'sis_spa' ),
					'id' => 'footer-widget-area',
					'description' => __( 'footer widget area', 'sis_spa' ),
					'before_widget' => '<div class="span3 footer_widget">',
					'after_widget' => '</div>',
					'before_title' => '<div class="widget_title"><h3 class="footer-widget-title">',
					'after_title' => '</h3></div>',					
				) );

			register_sidebar( array(
					'name' => __( 'Woocomerce Sidebar', 'sis_spa' ),
					'id' => 'woo-widget-area',
					'description' => __( 'Woocomerce Sidebar Widget Area', 'sis_spa' ),
					'before_widget' => '<div class="sidebar-widget">',
					'after_widget' => '</div>',
					'before_title' => '<div class="sidebar-widget-title"><h4>',
					'after_title' => '</h4></div>',					
				) );
			}	                     
			add_action( 'widgets_init', 'spa_widgets_init' );


			//enqueue  Scripts---------------------------------------------------------------------------------
			add_action('wp_enqueue_scripts','spa_enqueue_script'); 
			function spa_enqueue_script() {

				$current_options=get_option('spa_theme_options');				
				$default=get_template_directory_uri().'/css/'.$current_options['color_scheme_style'];
				wp_enqueue_style('spa-color-css', $default);				
				
				
				require_once('option_pannel/custom_style.php');				
				wp_enqueue_style('spa-custom-responsive', get_template_directory_uri().'/css/custom-responsive.css');
				wp_enqueue_style('spa-bootstrap', get_template_directory_uri().'/css/bootstrap.css');
				wp_enqueue_style('spa-bootstrap-responsive', get_template_directory_uri().'/css/bootstrap-responsive.css');
				wp_enqueue_style('spa-docs', get_template_directory_uri().'/css/docs.css');
				wp_enqueue_style('spa-flexslider', get_template_directory_uri().'/css/flexslider.css'); 
				wp_enqueue_style('spa-flexsliderdemo', get_template_directory_uri().'/css/flexslider-demo.css');  
				wp_enqueue_style('spa-font', get_template_directory_uri().'/css/font/font.css'); 
					 
				wp_enqueue_script('spa-menu', get_template_directory_uri().'/js/menu/menu.js',array('jquery'));
				wp_enqueue_script('spa-boot-menus', get_template_directory_uri().'/js/menu/bootstrap.min.js'); 
				wp_enqueue_script('spa-flexmain', get_template_directory_uri().'/js/flex/jquery.flexslider.js');
				wp_enqueue_script('spa-flexslider-setting', get_template_directory_uri().'/js/flex/flexslider-setting.php');
				wp_enqueue_script('spa-flexslider-setting', get_template_directory_uri().'/js/flex/flexslider-setting.js');			
				// webriti tab js 
				wp_enqueue_script('spa-webriti-tab-js', get_template_directory_uri().'/js/webriti-tab-js.js'); 
				
				/******** style switcher js and css *********/
				wp_enqueue_style('spa-switcher', get_template_directory_uri().'/css/switcher/switcher.css');
				wp_enqueue_style('spa-font-awesome', get_template_directory_uri().'/font-awesome/css/font-awesome.css');				
				wp_enqueue_script('spa-switcherjs', get_template_directory_uri().'/js/color_scheme/switcher.js'); 
				wp_enqueue_script('spa-spectrumjs', get_template_directory_uri().'/js/color_scheme/spectrum.js'); 
				/******** style switcher js and css end *********/				  
				 
             if(('spa_services' == get_post_type())||('spa_slider'==get_post_type())||('post'==get_post_type())){
			   wp_enqueue_script('spa-pikachoose', get_template_directory_uri().'/js/pikachoose/jquery.pikachoose.min.js');
			   wp_enqueue_script('spa-pikachoose1', get_template_directory_uri().'/js/pikachoose/pikachoose.js');
			   }
			}
	//Woocomerce Theme Support
	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
// Remove max_srcset_image_width added by priyanshu.
 function spasalon_remove_max_srcset_image_width( $max_width ) {
     return false;
 }
add_filter( 'max_srcset_image_width', 'spasalon_remove_max_srcset_image_width' );
?>
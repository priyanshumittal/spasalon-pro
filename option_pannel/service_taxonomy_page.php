<?php 
$current_options=get_option('spa_theme_options');
if(isset($_POST['spa_service_category_layout']))
{
	if($_POST['spa_service_category_layout'] == 1) 
	{	
		/*nonce field implement*/
	if ( empty($_POST) || !wp_verify_nonce($_POST['spa_home_nonce_customization'],'spa_customization_nonce_home') )
		{
		   _e('Sorry, your nonce did not verify.','spa');
		   exit;
		}
		else
		{ 
            $current_options['tax_service_item']= sanitize_text_field($_POST['tax_service_item']);
			
			/*update all field*/
			update_option('spa_theme_options' , stripslashes_deep($current_options));
		}
	}
	// restore defualt data 
	if($_POST['spa_service_category_layout'] == 2) 
	{	$current_options=get_option('spa_theme_options');	
		$current_options['tax_service_item']=1;
		update_option('spa_theme_options' , $current_options);
	}	
}
?>

<div class="block ui-tabs-panel ui-widget-content ui-corner-bottom" id="service_category_layout" aria-labelledby="ui-id-10" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
<form method="post"   id="spa_service_setting">
 <?php wp_nonce_field('spa_customization_nonce_home','spa_home_nonce_customization'); ?> 

<!-- code for site tegline title and content--> 
<div class="option option-input">
<h3><?php _e('Select Number of Service Category Layout option','sis_spa');?></h3>
			<?php $tax_service_item = $current_options['tax_service_item']; ?>
	<div class="section">
        <div class="element">			
			<select name="tax_service_item" class="webriti_inpute" >					
				<option value="1" <?php selected($tax_service_item, '1' ); ?>>1</option>
				<option value="2" <?php selected($tax_service_item, '2' ); ?>>2</option>
			</select>
		</div>
	</div>
</div>

<!-- code for product tegline title and content--> 
<!--end if product setting-->
	<input type="hidden" value="1" id="spa_service_category_layout" name="spa_service_category_layout" />
	<input type="button" class="button-framework save-options"  value= "<?php _e('Save Changes', 'sis_spa');?>" onclick="datasave_service_category()"/>									
	<input type="button" class="button-framework reset"   value="<?php _e('Restore Defaults','sis_spa');?>" onclick="reset_data_service_category()" />
	<div id="success_message_reset_service_category"  class="success_message" ><img style="padding-right:5px;" src="<?php echo  get_template_directory_uri();?>/option_pannel/images/icon_check.png" /><?php _e('Data reset sucessfully','sis_spa'); ?></div>
	<div id="success_message_save_ervice" class="success_message" ><img style="padding-right:5px;" src="<?php echo  get_template_directory_uri();?>/option_pannel/images/icon_check.png" /><?php _e('Data save sucessfully','sis_spa'); ?></div>	
	</form>
</div>
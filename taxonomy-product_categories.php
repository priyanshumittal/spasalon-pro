<?php	get_template_part('pink','header');
 $term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
	?>
<div class="container">
	<h2 class="spa-products-heading"><?php echo $term->name;  ?></h2> 
		<div  class="spa-Products">
			<div class="item">
				<?php				
				while(have_posts()): the_post();		
				$my_meta = get_post_meta($post->ID,'_my_meta',TRUE);
				if(isset($my_meta['link']))
				{	$meta_value_link = $my_meta['link']; }
				else
				{	$meta_value_link = get_permalink(); } 
				
				if(isset($my_meta['check']))
				{ $target ='target="_blank"';  }
				else 
				{ $target ='target="_self"';  }
				?>
				<span class="spa-products-thumbnail">
					<?php $defalt_arg =array('class' => "img-responsive" );?>
					<?php if(has_post_thumbnail()):?>
							<a <?php echo $target; ?> href="<?php echo $meta_value_link; ?>"title="<?php the_title(); ?>">
							<?php the_post_thumbnail('', $defalt_arg); ?>
							</a>
					<?php endif; ?>
					<h4><a <?php echo $target; ?> href="<?php echo $meta_value_link; ?>"><?php the_title(); ?></a></h4>
					<p><?php if(isset($my_meta['description']))echo $my_meta['description'];?></p><br />
					<a  class="spa-products-btn"><?php if(isset($my_meta['price'])) echo $my_meta['price']; ?></a>
				</span>
				<?php endwhile; ?>
			</div>
		</div>
</div><!--  container of products-->
<?php  get_footer(); ?>